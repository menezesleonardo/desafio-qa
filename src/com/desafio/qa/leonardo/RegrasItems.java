package com.desafio.qa.leonardo;


public enum RegrasItems {
	
	A(50,3,20,0,0),
	B(30, 2, 15, 0, 0),
	C(20, 0, 0, 0 ,0),
	D(15, 0, 0 ,0,0);

	private int preco;
	private int quatidadeParaDesconto;
	private int valorDesconto;
	private int quantidadeDeItems;
	private int valorTotalPorProduto;
	
	private RegrasItems(int preco, int quantidadeParaDesconto, int desconto, int quantidadeDeItems, int valorTotalPorItem) {
		this.preco = preco;
		this.quatidadeParaDesconto = quantidadeParaDesconto;
		this.valorDesconto = desconto;
		this.quantidadeDeItems = quantidadeDeItems;
		this.valorTotalPorProduto = valorTotalPorItem;
	}
	
	private int getPreco() {
		return this.preco;
	}
	
	private int getDesconto(int quantidade) {
		if (quantidade==quatidadeParaDesconto) {
			this.quantidadeDeItems = 0;
			return this.valorDesconto;
		}
		return 0;
	}
	
	private int getQuantidadeDeProdutos() {
		return quantidadeDeItems;
	}

	private void setQuantidadeDeProdutos(int quantidadeDeProdutos) {
		this.quantidadeDeItems = quantidadeDeProdutos;
	}

	private void setValorTotalPorProduto(int valorTotalPorProduto) {
		this.valorTotalPorProduto += valorTotalPorProduto - getDesconto(quantidadeDeItems);
	}
	
	public static int getTotal() {
		int total = 0;
		for (RegrasItems produto : RegrasItems.values()) {
			total += produto.valorTotalPorProduto;
		}
		return total;
	}
	
	public static void limparItems() {
		for (RegrasItems item : RegrasItems.values()) {
			item.valorTotalPorProduto = 0;
			item.quantidadeDeItems = 0;
		}
	}
	
	
	public static void addItem(String valorScanner) {
		try {
			RegrasItems.valueOf(valorScanner).setQuantidadeDeProdutos(RegrasItems.valueOf(valorScanner).getQuantidadeDeProdutos() + 1);
			RegrasItems.valueOf(valorScanner).setValorTotalPorProduto(RegrasItems.valueOf(valorScanner).getPreco());
		}catch (Exception e) {
			System.out.println("Item n�o existe!");
		}
	}
}
