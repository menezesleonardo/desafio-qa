package com.desafio.qa.leonardo;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class TestePreco {
	
	public int preco(String string) {
		return new Checkout().preco(string);
	}
	
	@Test
	public void testeIncremental() {
		
		Checkout checkOut = new Checkout();
		assertEquals( 0, checkOut.total());
		
		checkOut.scan("A"); assertEquals( 50, checkOut.total());
		checkOut.scan("B"); assertEquals( 80, checkOut.total());
		checkOut.scan("A"); assertEquals(130, checkOut.total());
		checkOut.scan("A"); assertEquals(160, checkOut.total());
		checkOut.scan("B"); assertEquals(175, checkOut.total());
	}
	
	@Test
	public void testeTotal() {
		 
		 assertEquals(  0, preco(""));
	     assertEquals( 50, preco("A"));
	     assertEquals( 80, preco("AB"));
	     assertEquals(115, preco("CDBA"));
	     assertEquals(100, preco("AA"));
	     assertEquals(130, preco("AAA"));
	     assertEquals(180, preco("AAAA"));
	     assertEquals(230, preco("AAAAA"));
	     assertEquals(260, preco("AAAAAA"));
	     assertEquals(160, preco("AAAB"));
	     assertEquals(175, preco("AAABB"));
	     assertEquals(190, preco("AAABBD"));
	     assertEquals(190, preco("DABABA"));
	}
}
