SENDO um usuário do WhatsApp
	QUERO deletar conversas
	PARA que NÃO seja mais exibido a conversa deletada em minha lista de conversas.
	
	Cenário_01 : Deletar conversa sem apagar mídia do aparelho
            DADO que o usuário esteja na aba de "Conversas" do WhatsApp
            E, tenha clicado e segurado em cima de uma conversa existente
			E, esteja visualizando que a conversa foi selecionada
			E, na parte superior da tela tenha sido exibido o "Icone deletar conversa"
            QUANDO clicar no "Icone deletar conversa" deverá ser exibido uma alerta com a mensagem "Apagar a conversa com #NOME_USUARIO_SELECIONADO# ?"
			E, ao selecionar a opção de "APAGAR"
            ENTÃO deve ser exibido uma mensagem "Por favor aguarde, enquanto a conversa é deletada..."
			E, ao fim da execução, a conversa deletada deve ser removida da lista, mas os arquivos da conversa devem permanecer no dispositivo.
			
	Cenário_02 : Deletar conversa sem apagar mídia do aparelho
            DADO que o usuário esteja na aba de "Conversas" do WhatsApp
            E, tenha clicado e segurado em cima de uma conversa existente
			E, esteja visualizando que a conversa foi selecionada
			E, na parte superior da tela tenha sido exibido o "Icone deletar conversa"
            QUANDO clicar no "Icone deletar conversa" deverá ser exibido uma alerta com a mensagem "Apagar a conversa com #NOME_USUARIO_SELECIONADO# ?"
			E, ao selecionar a opção "Apagar mídia do meu Aparelho" e a opção "APAGAR" 
            ENTÃO deve ser exibido uma mensagem "Por favor aguarde, enquanto a conversa é deletada"
			E, ao fim da execução a conversa deletada deve ser removida da lista e os arquivos devem da conversa também devem ser deletados do dispositivo.