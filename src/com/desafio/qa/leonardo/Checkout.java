package com.desafio.qa.leonardo;

public class Checkout {
	
	public Checkout() {
		RegrasItems.limparItems();
	}
	
	public int total() {
		return RegrasItems.getTotal();
	}
	
	public void scan(String string) {
		RegrasItems.addItem(string);
	}
	
	public int preco(String listaDeItems) {
		
		String arrayDeItems [] = listaDeItems.split("");
		
		for (String item : arrayDeItems) {
			RegrasItems.addItem(item);
		}
		
		return RegrasItems.getTotal();
	}
}
