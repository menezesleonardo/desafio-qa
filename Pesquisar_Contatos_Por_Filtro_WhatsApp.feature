SENDO um usuário do WhatsApp
	QUERO pesquisar contatos 
	PARA que seja exibido somente os contatos selecionados na pesquisa.
 
	Cenário_01 : Pesquisar contato pelo nome
            DADO que o usuário esteja na aba de "Conversas" do WhatsApp
            E, tenha clicado no botão flutuante com o ícone de conversas
			E, esteja visualizando a "lupa de pesquisa"
            QUANDO clicar na "lupa de pesquisa" e preencher o campo "pesquisar" com um nome de usuário OU alguma parte do nome
            ENTÃO a lista de usuários deve ser filtrada para que seja exibido apenas usuários que contenham o nome igual ou com parte da pesquisa realizada. 
			
	Cenário_02 : Pesquisar contato pelo n�mero de celular
            DADO que o usuário esteja na aba de "Conversas" do WhatsApp
            E, tenha clicado no botão flutuante com o ícone de conversas
			E, esteja visualizando a "lupa de pesquisa"
            QUANDO clicar na "lupa de pesquisa" e preencher o campo "pesquisar" com número de celular de usuário OU alguma parte do número
            ENTÃO a lista de usuários deve ser filtrada para que seja exibido apenas usuários que contenham o n�mero de celular igual ou com parte da pesquisa realizada.